#pragma once
#include "rawModel.h"
#include "..\textures\ModelTexture.h"
#include <vector>
class TexturedModel {
friend bool operator<(const TexturedModel& __x, const TexturedModel& __y);
public:	
	TexturedModel(){
		//this->rawModel = NULL;
		//this->texture = NULL;
	}
	TexturedModel(RawModel model, ModelTexture texture){
		this->rawModel = model;
        this->texture = texture;
	}

	
	RawModel getRawModel() const  {
		return rawModel;
	}
	ModelTexture getCModelTextures() const   {
		return texture;
	}
	ModelTexture* getModelTextures()  {
		return &texture;
	}
private:
	RawModel rawModel;
	ModelTexture texture;

};
bool operator<(const TexturedModel& __x, const TexturedModel& __y)
{
	return __x.getRawModel().getVaoId() < __y.getRawModel().getVaoId();
}
//TexturedModel::TexturedModel(RawModel& model, ModelTexture& texture) {
//	rawModel = model;
//	texture = texture;
//}

