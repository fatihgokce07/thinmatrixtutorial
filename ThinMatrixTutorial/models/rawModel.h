#pragma once
#pragma once
struct RawModel {
public:
	RawModel(int vaoId, int vertexCount) {
		this->vaoId = vaoId;
		this->vertexCount = vertexCount;
	}
	RawModel() {
	this->vaoId = 0;
	this->vertexCount = 0;
	}
	int getVaoId() const {
		return this->vaoId;
	}
	int getVertexCount() const {
		return this->vertexCount;
	}
private:
	int vaoId;
	int vertexCount;
};
