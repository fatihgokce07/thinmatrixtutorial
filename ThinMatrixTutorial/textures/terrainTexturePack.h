#pragma once
#include "terrainTexture.h"
struct TerrainTexturePack {
public:
	TerrainTexture backgroundTexture;
	TerrainTexture rTexture;
	TerrainTexture gTexture;
	TerrainTexture bTexture;
	TerrainTexturePack(TerrainTexture backgroundTexture, TerrainTexture rTexture, TerrainTexture gTexture,TerrainTexture bTexture){
		this->backgroundTexture = backgroundTexture;
		this->rTexture = rTexture;
		this->gTexture = gTexture;
		this->bTexture = bTexture;
	}
	TerrainTexturePack(){}
};