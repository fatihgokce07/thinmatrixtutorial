#pragma once
class ModelTexture {
public:
	float shineDamper = 1;
	float reflectivity = 0;
	bool hasTransparency = false;
	bool useFakeLighting = false;
	int numberOfRows = 1;
	ModelTexture(int id) {
		this->textureID = id;
	}
	ModelTexture() {
		this->textureID = -1;
	}
	int getID() {
		return textureID;
	}
	
private:
	int textureID;
	
};
