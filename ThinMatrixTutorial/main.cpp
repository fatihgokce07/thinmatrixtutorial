
#include <iostream>
#include <SDL2/SDL.h>

#include <glm/glm.hpp>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

#include <fstream>
#include <iostream>
#include "renderEngine\displayManager.h"
#include "renderEngine\Loader.h"
#include "renderEngine\entityRenderer.h"
#include <GL/glew.h>
#include "shaders\StaticShader.h"
#include "textures\ModelTexture.h"
#include "models\texturedModel.h"
#include "toolbox\maths.h"
#include "entities\entity.h"
#include "entities\Camera.h"
#include "renderEngine\objLoader.h"
#include "entities\light.h"
#include "renderEngine\masterRenderer.h"
#include "terrains\terrain.h"
#include "textures\terrainTexture.h"
#include "entities\player.h"
#include "renderEngine\keyboard.h"
#include "guis\guiRenderer.h"
#include "guis\guiTexture.h"
//for timer
#include <cstdlib>
#include <ctime>
#include <time.h>

static float genarateRandom() {
	
	float r = (float)(rand() / (100 % 10 + 1)) / RAND_MAX;
	return r;
}
int main(int argc, char** argv)
{

	SDL_Event e;
	bool isRunning = true;
	DisplayManager::createDisplay();

	Loader loader;
	TerrainTexture backgroundTexture(loader.loadTexture("grassy", 0));
	TerrainTexture rTexture(loader.loadTexture("dirt", 0));
	TerrainTexture gTexture(loader.loadTexture("pinkFlowers", 0));
	TerrainTexture bTexture(loader.loadTexture("path", 0));

	TerrainTexturePack texturePack(backgroundTexture, rTexture, gTexture, bTexture);
	TerrainTexture blendMap(loader.loadTexture("blendMap", 0));




	RawModel model = OBJLoader::loadObjModel("tree", loader);

	TexturedModel staticModel{ model,ModelTexture(loader.loadTexture("tree",0)) };

	TexturedModel grass{ OBJLoader::loadObjModel("grassModel", loader),ModelTexture(loader.loadTexture("grassTexture",0)) };
	grass.getModelTextures()->hasTransparency = true;
	grass.getModelTextures()->useFakeLighting = true;
	TexturedModel flower{ OBJLoader::loadObjModel("grassModel", loader),ModelTexture(loader.loadTexture("flower",0)) };
	flower.getModelTextures()->hasTransparency = true;
	flower.getModelTextures()->useFakeLighting = true;
	ModelTexture fernTextureAtlas(loader.loadTexture("fern",0));
	fernTextureAtlas.numberOfRows=2;
	TexturedModel fern{ OBJLoader::loadObjModel("fern", loader),fernTextureAtlas };
	fern.getModelTextures()->hasTransparency = true;
	TexturedModel bobble{OBJLoader::loadObjModel("lowPolyTree", loader),
		ModelTexture(loader.loadTexture("lowPolyTree", 0))};

	TexturedModel box{ OBJLoader::loadObjModel("box", loader),
		ModelTexture(loader.loadTexture("box", 0)) };
	std::vector<Entity> entities;
	srand(time(NULL));
	Terrain terrain(-0.5, -1, loader, texturePack, blendMap,"heightmap");
	//Terrain terrain2(-1, -1, loader, texturePack, blendMap);
	for (int i = 0; i < 400; i++) {	
		float f2 = genarateRandom() * 400 - 200;
		if (i % 2 == 0) {
			float x = genarateRandom() * 800 - 400;
			float z = genarateRandom() * -600;
			float y = terrain.getHeightOfTerrain(x, z);
			int whichIndex = (int)(genarateRandom() * 4);
			Entity entity3(fern, glm::vec3(x,y,z), whichIndex, 0, genarateRandom() * 360, 0, 0.9);
			entities.push_back(entity3);
		}
		if (i % 5 == 0) {
			float x = genarateRandom() * 800 - 400;
			float z = genarateRandom() * -600;
			float y = terrain.getHeightOfTerrain(x, z);
		
			

			Entity entity4(bobble, glm::vec3(x,y,z), 0, genarateRandom()*360, 0, genarateRandom() * 0.1 + 0.6);		
			
			Entity entity(staticModel, glm::vec3(x,y,z), 0, 0, 0, genarateRandom() * 1 + 4);
			entities.push_back(entity);
		
			entities.push_back(entity4);
		}
		
	}
	Entity entity2(staticModel, glm::vec3(0, 0, -25), 0, 0, 0, 1);
	Light light(glm::vec3(3000, 2000, 2000), glm::vec3(1, 1, 1));
	

	
	

	MasterRenderer renderer2;
	RawModel bunnyModel = OBJLoader::loadObjModel("person", loader);
	TexturedModel stanfordBunny(bunnyModel,ModelTexture(loader.loadTexture("playerTexture",0)));

	Player player(stanfordBunny, glm::vec3(100, 0, -50), 0, 180, 0, 0.6f);
	Camera camera(&player,player);

	std::vector<GuiTexture> guis;
	GuiTexture gui(loader.loadTexture("socuwan",0),
		glm::vec2(0.50f, 0.50f), glm::vec2(0.25f, 0.25f));
	GuiTexture gui2(loader.loadTexture("thinmatrix",0),
		glm::vec2(0.30f, 0.74f),glm::vec2(0.4f, 0.4f));

	guis.push_back(gui2);
	guis.push_back(gui);
	GuiRenderer guiRenderer(loader);
	while (isRunning)
	{
		
		Keyboard::readInput();		
		camera.move();
		player.move(terrain);
		renderer2.processEntity(player);
		renderer2.processTerrain(terrain);
		//renderer2.processTerrain(terrain2);
		for (Entity& entity : entities) {
			renderer2.processEntity(entity);
		}
		renderer2.render(light, camera);
		guiRenderer.render(guis);
		DisplayManager::updateDisplay();
		
		if (Keyboard::keyboards[SDL_QUIT])
			isRunning = false;
	
	}
	guiRenderer.cleanUp();
	//shader.clenaUp();
	renderer2.cleanUp();
	loader.cleanUp();
	DisplayManager::closeDisplay();

	return 0;
}

