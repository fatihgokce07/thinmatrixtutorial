#pragma once

#include <map>
#include <vector>
#include <GL/glew.h>
#include "..\models\rawModel.h"
#include "..\models\texturedModel.h"
#include "..\entities\entity.h"
#include "..\shaders\StaticShader.h"
#include "..\toolbox\maths.h"
#include "..\renderEngine\displayManager.h"

#include <glm/glm.hpp>

class EntityRenderer {
public:
	EntityRenderer(StaticShader shader, glm::mat4 projectionMatrix) {
		this->shader = shader;
		
		//createProjectionMatrix();
		shader.start();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.stop();
	}
	EntityRenderer(){}
	void render(std::map<TexturedModel, std::vector<Entity>> entities);
	
private:
	
	StaticShader shader;
	void bindTextures(std::vector<ModelTexture> textures,StaticShader shader) {
		for (auto i = 0; i < textures.size(); i++) {
			auto texture = textures[i];
			shader.loadShineVariables(texture.shineDamper, texture.reflectivity);
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, textures[i].getID());			
		}
	}
	
	void prepareTexturedModel(const TexturedModel& tmodel);
	void unbindTexturedModel();
	void prepareInstance(Entity& entity);
};




void EntityRenderer::render(std::map<TexturedModel, std::vector<Entity>> entities) {


	for (auto& model : entities) {
		prepareTexturedModel(model.first);
		auto batch = model.second;
		for (auto& entity : batch) {
			prepareInstance(entity);
			//glDrawArrays(GL_TRIANGLES, 0, model.getVertexCount());
			glDrawElements(GL_TRIANGLES, model.first.getRawModel().getVertexCount(), GL_UNSIGNED_INT, 0);//for indexed buffer
		}
		unbindTexturedModel();
	}
}
void EntityRenderer::prepareTexturedModel(const TexturedModel& tmodel) {

	RawModel rawModel = tmodel.getRawModel();
	glBindVertexArray(rawModel.getVaoId());
	//for input shader
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	ModelTexture texture = tmodel.getCModelTextures();
	if (texture.hasTransparency) {
		DisplayManager::disableCulling();
	}
	shader.loadNumberOfRows(texture.numberOfRows);
	shader.loadFakeLightingVariable(texture.useFakeLighting);
	shader.loadShineVariables(texture.shineDamper, texture.reflectivity);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,texture.getID());
	//bindTextures(tmodel.getModelTextures(), shader);
}
void EntityRenderer::unbindTexturedModel() {
	DisplayManager::enableCulling();
	
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}
void EntityRenderer::prepareInstance(Entity& entity) {
	glm::mat4 transformationMatrix = Maths::createTransformationMatrix(entity.position, entity.rotX, entity.rotY, entity.rotZ, entity.scale);
	shader.loadTransformationMatrix(transformationMatrix);
	shader.loadOffset(entity.getTextureXOffset(), entity.getTextureYOffset());
}
