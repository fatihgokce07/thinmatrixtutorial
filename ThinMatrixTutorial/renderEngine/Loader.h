#pragma once
#include "models/rawModel.h"
#include <GL/glew.h>
#include <vector>
#include "stb_image.h"
#include <iostream>
class Loader {
public:
	Loader() {
		/*this->vaoId = 0;
		this->vboId = 0;*/
	}
	RawModel loadToVAO(std::vector<glm::vec3> positions,std::vector<glm::vec2> textureCoords, std::vector<glm::vec3> normals,std::vector<int> indices) {
		createVAO();
		bindIndicesBuffer(indices);
		storeDataInAttributeList(0,3, positions);
		storeDataInAttributeList(1,2, textureCoords);
		storeDataInAttributeList(2, 3, normals);
		unbindVAO();
		//int vertexCount = sizeof(positions) / sizeof(float);
	
		return RawModel(vaoId, indices.size());
	}
	RawModel loadToVAO(std::vector<glm::vec2> positions) {
		createVAO();
		storeDataInAttributeList(0, 2, positions);;
		unbindVAO();
		return RawModel(vaoId, positions.size());
	}
	void cleanUp() {
		for (auto i = 0; i < vaos.size(); i++) {
			glDeleteVertexArrays(1, &vaos[i]);
		}
		for (auto i = 0; i < vbos.size(); i++) {
			glDeleteBuffers(1, &vbos[i]);
		}
		for (auto i = 0; i < textures.size(); i++) {
			glDeleteTextures(1, &textures[i]);
		}
	}
	int loadTexture(std::string fileName, int textureIndex);
private:
	std::vector<GLuint> vaos;
	std::vector<GLuint> vbos;
	std::vector<GLuint> textures;
	GLuint vaoId;
	GLuint vboId;
	GLuint vboIndexBuffer;
	//void operator=(const Loader& mesh) {}
	//Loader(const Loader& mesh) {}
	void createVAO() {
		
		glGenVertexArrays(1, &vaoId);
		vaos.push_back(vaoId);
		glBindVertexArray(vaoId);
		//return vaoId;

	}
	template<typename T>
	void storeDataInAttributeList(int attributeNumber,int coordinateSize, const std::vector<T>& data) {
		
		glGenBuffers(1, &vboId);
		vbos.push_back(vboId);
		glBindBuffer(GL_ARRAY_BUFFER, vboId);
		glBufferData(GL_ARRAY_BUFFER, sizeof(data[0])*data.size(), &data[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(attributeNumber);
		glVertexAttribPointer(attributeNumber, coordinateSize, GL_FLOAT, GL_FALSE, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	void unbindVAO() {
		glBindVertexArray(0);
	}
	void bindIndicesBuffer(const std::vector<int>& indices) {
		
		glGenBuffers(1, &vboIndexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0])*indices.size(), &indices[0], GL_STATIC_DRAW);
		vbos.push_back(vboIndexBuffer);
	}


};
int Loader::loadTexture(std::string fileName,int textureIndex) {
	int width, height, numComponents;
	std::string fp = "./res/" + fileName + ".png";
	unsigned char* data = stbi_load((fp).c_str(), &width, &height, &numComponents, 4);

	if (data == NULL)
		std::cerr << "Unable to load texture: " << fileName << std::endl;
	GLuint texture;
	glGenTextures(1, &texture);
	textures.push_back(texture);
	int d = GL_TEXTURE0 + textureIndex;
	glActiveTexture(d);
	glBindTexture(GL_TEXTURE_2D, texture);
	
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.4f);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(data);
	return texture;
};
