#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <GL/glew.h>
#include "..\toolbox\maths.h"
#include "..\shaders\StaticShader.h"
//#include "entityRenderer.h"
#include <map>
#include <set>
#include "..\models\texturedModel.h"
#include "..\entities\light.h"
#include "..\shaders\terrainShader.h"
#include "terrainRenderer.h"

class MasterRenderer {
	
public:
	
	MasterRenderer();
	void cleanUp();
	void render(Light& sun,Camera& camera);
	void processEntity(Entity& entity);
	void prepare();
	void createProjectionMatrix();
	void processTerrain(Terrain terrain);
	
private:
	glm::mat4 projectionMatrix;
	const float FOV = 70;
	const float NEAR_PLANE = 0.1f;
	const float FAR_PLANE = 1000;
	inline static const float RED = 0.5f;
	inline static const float BLUE = 0.5f;
	inline static const float GREEN = 0.5f;
	StaticShader shader;
	//Renderer renderer = Renderer(shader);
	EntityRenderer renderer;// { shader, projectionMatrix };
	std::map<TexturedModel, std::vector<Entity>> entities;
	TerrainRenderer terrainRenderer;
	TerrainShader terrainShader;
	std::vector<Terrain>terrains;
};
MasterRenderer::MasterRenderer() {
	DisplayManager::enableCulling();
	createProjectionMatrix();
	renderer = EntityRenderer(shader, projectionMatrix);
	terrainRenderer = TerrainRenderer(terrainShader, projectionMatrix);
}
void MasterRenderer::cleanUp() {
	shader.clenaUp();
	terrainShader.clenaUp();
}
void MasterRenderer::render(Light& sun, Camera& camera) {
	prepare();
	shader.start();
	shader.loadSkyColor(RED, GREEN, BLUE);
	shader.loadLight(sun);
	shader.loadViewMatrix(camera);
	renderer.render(entities);
	shader.stop();
	terrainShader.start();
	terrainShader.loadSkyColor(RED, GREEN, BLUE);
	terrainShader.loadLight(sun);
	terrainShader.loadViewMatrix(camera);
	terrainRenderer.render(terrains);
	terrainShader.stop();
	terrains.clear();
	entities.clear();
	
}
void MasterRenderer::processEntity(Entity& entity) {
	TexturedModel entityModel = entity.getTextureModel();
	std::vector<Entity>& batch = entities[entityModel];
	//auto batch = entities.at(entityModel);// [entityModel];
	if (batch.size() > 0) {
		batch.push_back(entity);
	}
	else {
		std::vector<Entity> newBatch;
		newBatch.push_back(entity);
		entities[entityModel] = newBatch;
	}

}
void MasterRenderer::prepare() {
	//The Z - Buffer
	//	The solution to this problem is to store the depth(i.e.�Z�) component of each fragment in a buffer, 
	//and each and every time you want to write a fragment, you first check if you should(i.e the new fragment is closer than the previous one).

	//	You can do this yourself, but it�s so much simpler to just ask the hardware to do it itself :
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(RED, GREEN, BLUE, 1);

}
void MasterRenderer::createProjectionMatrix() {
	float aspectRatio = (float)DisplayManager::getWidth() / (float)DisplayManager::getHeight();
	float y_scale = (float)((1.0f / tan(Maths::toRadian(FOV / 2.0f))) * aspectRatio);
	float x_scale = y_scale / aspectRatio;
	float frustum_length = FAR_PLANE - NEAR_PLANE;

	projectionMatrix = glm::mat4();
	projectionMatrix[0][0] = x_scale;
	projectionMatrix[1][1] = y_scale;
	projectionMatrix[2][2] = -((FAR_PLANE + NEAR_PLANE) / frustum_length);
	projectionMatrix[2][3] = -1;
	projectionMatrix[3][2] = -((2 * NEAR_PLANE * FAR_PLANE) / frustum_length);
	projectionMatrix[3][3] = 0;
	//glm::mat4 Projection = glm::perspective(glm::radians(70.0f), 4.0f / 3.0f, 0.1f, 1000.f);
	//projectionMatrix = Projection;
}
void MasterRenderer::processTerrain(Terrain terrain) {
	terrains.push_back(terrain);
}


