#pragma once
#include <glm/glm.hpp>
#include <vector>
#include "..\models\rawModel.h"
#include "Loader.h"
#include <string> 
class  OBJLoader {
public:
	static RawModel loadObjModel(const std::string& fileName, Loader loader);
private:
	static std::vector<std::string> split(const std::string &text,const char& sep);
	static void processVertex(std::vector<std::string>& vertexData, std::vector<int>& indices,
		std::vector<glm::vec2>& textures, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& textureArray,
		std::vector<glm::vec3>& normalsArray);
};
RawModel OBJLoader::loadObjModel(const std::string& fileName, Loader loader) {
	std::ifstream reader;
	std::string line;
	std::string fn = "./res/" + fileName + ".obj";
	reader.open(fn.c_str());
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> textures;
	std::vector<glm::vec3> normals;
	std::vector<int> indices;
	

	std::vector<glm::vec3> verticesArray;
	std::vector<glm::vec3>  normalsArray;
	std::vector<glm::vec2>  textureArray;
	std::vector<int>  indicesArray ;
	if (reader.is_open()) {
		while (reader.good()) {
			getline(reader, line);
			/*if (line == "")
				break;*/
		    std::vector<std::string> currentLine = split(line, ' ');
		
			
			if (line.find("v ", 0) != std::string::npos) {
				glm::vec3 vertex{ std::stof(currentLine[1]),std::stof(currentLine[2]),std::stof(currentLine[3]) };
				vertices.push_back(vertex);
			}
			else if (line.find("vt",0) != std::string::npos) {
				glm::vec2 texture{ std::stof(currentLine[1]),std::stof(currentLine[2]) };
				textures.push_back(texture);
			}
			else if (line.find("vn",0) != std::string::npos) {
				glm::vec3 normal{ std::stof(currentLine[1]),std::stof(currentLine[2]),std::stof(currentLine[3]) };
				normals.push_back(normal);
			}
			else if (line[0] == 'f') {
				textureArray = std::vector<glm::vec2>(vertices.size());  //[vertices.size() * 2];
				normalsArray = std::vector<glm::vec3>(vertices.size());//new float[vertices.size() * 3];
				break;
				
			   
			}
			
		}
		do {
			if (line.find("f ", 0) == std::string::npos) {
				getline(reader, line);
				continue;
			}
				
			std::vector<std::string> currentLine = split(line, ' ');
			std::vector<std::string> vertex1 = split(currentLine[1], '/');
			std::vector<std::string> vertex2 = split(currentLine[2], '/');

			std::vector<std::string> vertex3;
			if (currentLine.size() > 2) {
				vertex3 = split(currentLine[3], '/');
			}
			processVertex(vertex1, indices, textures, normals, textureArray, normalsArray);
			processVertex(vertex2, indices, textures, normals, textureArray, normalsArray);
			if (vertex3.size()>0)
				processVertex(vertex3, indices, textures, normals, textureArray, normalsArray);
			getline(reader, line);
			if (line == "")
				break;

			
		} while (reader.good());
			  
	}
	/*for (int i = 0; i<indices.size(); i++) {
		indicesArray[i] = indices.get(i);
	}*/
	RawModel model = loader.loadToVAO(vertices, textureArray, normalsArray, indices);
	return model;

}
void OBJLoader::processVertex(std::vector<std::string>& vertexData, std::vector<int>& indices,
	std::vector<glm::vec2>& textures, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& textureArray,
	std::vector<glm::vec3>& normalsArray) {
	int currentVertexPointer = std::stoi(vertexData[0]) - 1;
	indices.push_back(currentVertexPointer);
	if (vertexData[1] != "") {
		glm::vec2 currentTex = textures[std::stoi(vertexData[1]) - 1];
		textureArray[currentVertexPointer] = currentTex;
		textureArray[currentVertexPointer].y = 1 - currentTex.y;
	}

	
	//textureArray[currentVertexPointer * 2] = currentTex.x;
	//opengl starts from top left of a texture whereas blender seems to start from the bottom left
	//textureArray[currentVertexPointer * 2 + 1] = 1 - currentTex.y;
	if(vertexData.size()>2) {
	glm::vec3 currentNorm = normals[std::stoi(vertexData[2]) - 1];
	normalsArray[currentVertexPointer] = currentNorm;
	/*normalsArray[currentVertexPointer * 3] = currentNorm.x;
	normalsArray[currentVertexPointer * 3 + 1] = currentNorm.y;
	normalsArray[currentVertexPointer * 3 + 2] = currentNorm.z;*/
	}
}
std::vector<std::string> OBJLoader::split(const std::string &text,const char& sep) {
	std::vector<std::string> tokens;
	std::size_t start = 0, end = 0;
	while ((end = text.find(sep, start)) != std::string::npos) {
		tokens.push_back(text.substr(start, end - start));
		start = end + 1;
	}
	tokens.push_back(text.substr(start));
	return tokens;
}

