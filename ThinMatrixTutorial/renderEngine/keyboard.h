#pragma once
#pragma once
#include <SDL2/SDL.h>
#include <map>
#include <iostream>
#include "displayManager.h"
class Keyboard {
public:
	void static readInput();
	bool static rightButtonClicked();
	bool static leftButtonClicked();
	static float getMouseDeltaX();
	static float getMouseDeltaY();
	inline static std::map<int, bool> keyboards;
	inline static SDL_Event e;
	inline static bool scroolUp;
	inline static bool startMouseWheel = false;

private:
	
	inline static bool isMouseClick = false;
	inline static SDL_MouseButtonEvent clickedButtonType;
	static float mouseDeltaX;
	inline static float lastDeltaX=0;
	inline static float mouseDeltaY;
	inline static float lastDeltaY = 0;
};
float Keyboard::mouseDeltaX = 0;
void Keyboard::readInput() {
	while (SDL_PollEvent(&e))
	{		
		switch (e.type)
		{
		case SDL_KEYDOWN:
			keyboards[e.key.keysym.sym] = true;
			break;

		case SDL_KEYUP:
			keyboards[e.key.keysym.sym] = false;
			break;
		case SDL_QUIT:
			keyboards[SDL_QUIT] = true;
			break;
		case SDL_MOUSEBUTTONDOWN:
		
			keyboards[e.button.button] = true;
			break;
		case SDL_MOUSEBUTTONUP:
			
			keyboards[e.button.button] = false;
			
			break;
		case SDL_MOUSEMOTION:		
			DisplayManager::setTitle(e.motion.x, e.motion.y);	
			if (lastDeltaX != 0) {
				mouseDeltaX = e.motion.x - lastDeltaX;
			}
			if (lastDeltaY != 0) {
				mouseDeltaY = e.motion.y - lastDeltaY;
			}
			lastDeltaX = e.motion.x;
			lastDeltaY = e.motion.y;
			break;
		case SDL_MOUSEWHEEL:		
			if (e.wheel.y < 0) {
				scroolUp = false;
				startMouseWheel = true;			
			}				
			else {			
				scroolUp = true;
				startMouseWheel = true;
			}				
			break;		
		default:
			break;
		}
	}
}
bool  Keyboard::rightButtonClicked() {
	return keyboards[SDL_BUTTON_RIGHT];
}
bool  Keyboard::leftButtonClicked() {
	return keyboards[SDL_BUTTON_LEFT];
}
float Keyboard::getMouseDeltaX() {
	return mouseDeltaX;
}
float Keyboard::getMouseDeltaY() {
	return mouseDeltaY;
}