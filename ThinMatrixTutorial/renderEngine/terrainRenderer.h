#pragma once
#include "..\models\rawModel.h"
#include "..\models\texturedModel.h"
#include <glm\glm.hpp>
#include <GL\glew.h>
#include "..\shaders\terrainShader.h"
#include "..\toolbox\maths.h"
#include "..\textures\ModelTexture.h"
#include "..\terrains\terrain.h"
#include <vector>
class TerrainRenderer {
public:
	
	TerrainRenderer(TerrainShader& shader2, glm::mat4 projectionMatrix) {
		this->shader = shader2;
		this->shader.start();
		this->shader.loadProjectionMatrix(projectionMatrix);
		this->shader.connectTextureUnits();
		this->shader.stop();
	}
	TerrainRenderer() {}
	void render(std::vector<Terrain> terrains);
private:
	TerrainShader shader;
	void prepareTerrain(Terrain terrain);
	void unbindTexturedModel();
	void loadModelMatrix(Terrain terrain);
	void bindTextures(Terrain terrain);
};
//TerrainRenderer::TerrainRenderer(TerrainShader shader, glm::mat4 projectionMatrix) {
//	
//}
void TerrainRenderer::render(std::vector<Terrain> terrains) {
	for (auto& model : terrains) {
		prepareTerrain(model);
		loadModelMatrix(model);
		glDrawElements(GL_TRIANGLES, model.getModel().getVertexCount(), GL_UNSIGNED_INT, 0);//for indexed buffer
		unbindTexturedModel();
	}
}
void TerrainRenderer::prepareTerrain(Terrain terrain) {
	RawModel rawModel = terrain.getModel();
	glBindVertexArray(rawModel.getVaoId());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	bindTextures(terrain);
	shader.loadShineVariables(1, 0);
	
}
void TerrainRenderer::unbindTexturedModel() {
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}
void TerrainRenderer::loadModelMatrix(Terrain terrain) {
	glm::mat4 transformationMatrix = Maths::createTransformationMatrix(
		glm::vec3(terrain.getX(), 0, terrain.getZ()), 0, 0, 0, 1);
	shader.loadTransformationMatrix(transformationMatrix);
}
void TerrainRenderer::bindTextures(Terrain terrain) {
	TerrainTexturePack texturePack = terrain.getTexturePack();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texturePack.backgroundTexture.textureID);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texturePack.rTexture.textureID);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, texturePack.gTexture.textureID);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, texturePack.bTexture.textureID);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, terrain.getBlendMap().textureID);
}