#pragma once
#include <GL/glew.h>
#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <time.h>
#include <chrono>
#include <sstream>
class DisplayManager {
public:
	static void print() {
	
		std::cout << "deneme:" <<WIDTH<< std::endl;
	}
	static void createDisplay() {
		SDL_Init(SDL_INIT_EVERYTHING);

		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		std::string title = "Tutorial 1";
		m_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL);
		m_glContext = SDL_GL_CreateContext(m_window);
		
		glewExperimental = true; // Needed for core profile
		GLenum res = glewInit();
		if (res != GLEW_OK)
		{
			std::cerr << "Glew failed to initialize!" << std::endl;
		}
		/*glEnable(GL_DEPTH_TEST);

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);*/
	
		glViewport(0, 0, WIDTH, HEIGHT);

	}
	static void closeDisplay() {
		SDL_GL_DeleteContext(m_glContext);
		SDL_DestroyWindow(m_window);
		SDL_Quit();
	}
	static void updateDisplay() {
		SDL_GL_SwapWindow(m_window);
		SDL_Delay(1);
		long currentFrameTime = getCurrentTime();
		delta = (currentFrameTime - lastFrameTime) / 1000.0f;
		//System.out.println("delta:"+delta);
		lastFrameTime = getCurrentTime();
	}
	static int getWidth()  {
		return WIDTH;
	}
	static int getHeight() {
		return WIDTH;
	}
	static void enableCulling() {
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
	static void disableCulling() {
		glDisable(GL_CULL_FACE);
		glDisable(GL_BACK);
	}
	static float getFrameTimeSeconds() {
	
		return delta;
	}
	static void setTitle(int mouseX, int mouseY) {
		std::stringstream ss;
		ss << "X: " << mouseX << " Y: " << mouseY;
		SDL_SetWindowTitle(m_window, ss.str().c_str());
	}
private:
	const static int WIDTH = 1280;
	const static int HEIGHT = 720;
	static SDL_Window* m_window;
	static SDL_GLContext m_glContext;
	inline static long lastFrameTime;
	inline static float delta;
	static long getCurrentTime() {
		/*auto start = std::chrono::system_clock::now();
	
		auto end = std::chrono::system_clock::now();
		
		std::chrono::duration<long> elapsed_seconds = end - start;*/
		/*typedef std::chrono::high_resolution_clock Time;
		typedef std::chrono::milliseconds ms;
		typedef std::chrono::duration<float> fsec;
		auto t0 = Time::now();
		auto t1 = Time::now();
		fsec fs = t1 - t0;
		ms d = std::chrono::duration_cast<ms>(fs);*/
		//std::cout << fs.count() << "s\n";
		//std::cout << d.count() << "ms\n";
		long d = SDL_GetTicks();
		return d;
	}
};

SDL_Window* DisplayManager::m_window = nullptr;
SDL_GLContext DisplayManager::m_glContext = nullptr;