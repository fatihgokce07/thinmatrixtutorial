#pragma once
#define GLM_ENABLE_EXPERIMENTAL 
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include "../entities/Camera.h"
class Maths {
public:
	
	static glm::mat4 createTransformationMatrix(glm::vec3 translation, float rx, float ry,
		float rz, float scale) {
		glm::mat4 matrix = glm::mat4(1.0f);//idetity matrix
	
		matrix = matrix * glm::translate(translation) ;
		matrix = matrix * glm::rotate(glm::radians(rx), glm::vec3(1.0, 0.0, 0.0))  ;
		matrix = matrix * glm::rotate(toRadian(ry), glm::vec3(0.0, 1.0, 0.0))  ;
		matrix = matrix * glm::rotate(toRadian(rz), glm::vec3(0.0, 0.0, 1.0));
		matrix = matrix * glm::scale(glm::vec3(scale, scale, scale)); // * matrix ;
		
		return matrix;
		/*glm::mat4 posMat = glm::translate(translation);
		glm::mat4 scaleMat = glm::scale(glm::vec3(scale, scale, scale));
		glm::mat4 rotX = glm::rotate(rx, glm::vec3(1.0, 0.0, 0.0));
		glm::mat4 rotY = glm::rotate(ry, glm::vec3(0.0, 1.0, 0.0));
		glm::mat4 rotZ = glm::rotate(rz, glm::vec3(0.0, 0.0, 1.0));
		glm::mat4 rotMat = rotX * rotY * rotZ;

		return posMat * rotMat * scaleMat;*/
	}
	static glm::mat4 createTransformationMatrix(glm::vec2& translation, glm::vec2& scale ) {
		glm::mat4 matrix = glm::mat4(1.0f);//idetity matrix
		
		matrix = matrix * glm::translate(glm::vec3(translation.x,translation.y,1));		
		matrix = matrix * glm::scale(glm::vec3(scale.x, scale.y, 1)); // * matrix ;

		return matrix;		
	}
	static glm::mat4 createViewMatrix(Camera camera) {
		glm::mat4 matrix = glm::mat4(1.0f);
	
		matrix = matrix * glm::rotate(glm::radians(camera.getPitch()), glm::vec3(1.0, 0.0, 0.0))  ;
		matrix = matrix * glm::rotate(glm::radians(camera.getYaw()), glm::vec3(0.0, 1.0, 0.0)) ;
	
		glm::vec3 cameraPos = camera.getPosition();
		glm::vec3 negativeCameraPos = glm::vec3(-cameraPos.x, -cameraPos.y, -cameraPos.z);
		matrix = matrix * glm::translate(negativeCameraPos);//* matrix ;
		return matrix;
		
		/*glm::mat4 matrix = glm::lookAt(camera.getPosition(), camera.getPosition() + glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f));*/
		//return matrix;
	}
	static float toRadian(const float& angle) {
		return (Maths::PI * angle) / 180;
	}
	inline const static float  PI = 3.14159265358979f;
private:
	

};
//const float  PI_F = 3.14159265358979f;