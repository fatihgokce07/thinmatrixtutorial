#pragma once
#ifndef GUI_TEXTURE_H
#define GUI_TEXTURE_H
#include <glm/glm.hpp>
class GuiTexture {
public:
	GuiTexture(int texture, glm::vec2 position, glm::vec2 scale) :texture(texture), position(position), scale(scale) {

	}
	int& getTexture() {
		return this->texture;
	}
	glm::vec2& getPosition() {
		return this->position;
	}
	glm::vec2& getScale() {
		return this->scale;
	}
private:
	int texture;
	glm::vec2 position;
	glm::vec2 scale;
};
#endif // !GUI_TEXTURE_H

