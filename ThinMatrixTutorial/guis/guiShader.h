#pragma once
#ifndef GUI_SHADER_H
#define GUI_SHADER_H
#include <glm/glm.hpp>
#include "..\shaders\ShaderProgram.h"
class GuiShader : public ShaderProgram {
public:
	GuiShader();
	void loadTransformation(glm::mat4& matrix);
private:
	const static std::string VERTEX_FILE ;
	const static std::string FRAGMENT_FILE ;

	int location_transformationMatrix;
protected:
	void bindAttributes() {
		ShaderProgram::bindAttribute(0, "position");	
	}
	void getAllUniformLocations();
};
const std::string GuiShader::VERTEX_FILE = "./guis/guiVertexShader.glsl";
const std::string GuiShader::FRAGMENT_FILE = "./guis/guiFragmentShader.glsl";
GuiShader::GuiShader() :ShaderProgram(VERTEX_FILE, FRAGMENT_FILE) {
	bindAttributes();
	getAllUniformLocations();
};
void GuiShader::getAllUniformLocations() {
	location_transformationMatrix = getUniformLocation("transformationMatrix");
}
void GuiShader::loadTransformation(glm::mat4& matrix) {
	loadMatrix(location_transformationMatrix, matrix);
}
#endif // !GUI_SHADER_H