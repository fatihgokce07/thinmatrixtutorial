#pragma once
#include <glm\glm.hpp>
#include "..\models\rawModel.h"
#include "guiShader.h"
#include "..\renderEngine\Loader.h"
#include "guiTexture.h"
#include <vector> 
#include "..\toolbox\maths.h"
class GuiRenderer {
public:
	GuiRenderer(Loader loader);
	void render(std::vector<GuiTexture> guis);
	void cleanUp();
private:
	RawModel quad;
	GuiShader shader;
};
GuiRenderer::GuiRenderer(Loader loader) {
	std::vector<glm::vec2> positions
	{ glm::vec2(-1,1),glm::vec2(-1,-1),glm::vec2(1,1),glm::vec2(1,-1)};
	quad = loader.loadToVAO(positions);
	shader = GuiShader();
}
void GuiRenderer::render(std::vector<GuiTexture> guis) {
	shader.start();
	glBindVertexArray(quad.getVaoId());
	glEnableVertexAttribArray(0);
	//for transparency
	glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);
	//end transparency
	// to show behind image
	glDisable(GL_DEPTH_TEST);
	for (auto& gui : guis) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, gui.getTexture());
		glm::mat4 matrix = Maths::createTransformationMatrix(gui.getPosition(), gui.getScale());
		shader.loadTransformation(matrix);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, quad.getVertexCount());
	}
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
	shader.stop();
}
void GuiRenderer::cleanUp() {
	shader.clenaUp();
}
