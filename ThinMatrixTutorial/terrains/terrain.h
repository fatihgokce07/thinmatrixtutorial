#pragma once
#include "..\models\texturedModel.h"
#include "..\renderEngine\Loader.h"
#include <vector>
#include <glm\glm.hpp>
#include "..\textures\terrainTexturePack.h"
#include <SDL2\SDL_image.h>
class Terrain {
public:
	Terrain(float gridX, float gridZ, Loader loader, TerrainTexturePack texturePack
		, TerrainTexture blendMap, std::string heighMap) {
		this->texturePack = texturePack;
		this->blendMap = blendMap;
		this->x = gridX * SIZE;
		this->z = gridZ * SIZE;
		this->model = generateTerrain(loader, heighMap);
	}
	float getX() {
		return x;
	}
	float getZ() {
		return z;
	}
	RawModel getModel() {
		return model;
	}
	TerrainTexturePack getTexturePack() {
		return texturePack;
	}
	void setTexturePack(TerrainTexturePack texturePack) {
		this->texturePack = texturePack;
	}

	TerrainTexture getBlendMap() {
		return blendMap;
	}

	void setBlendMap(TerrainTexture blendMap) {
		this->blendMap = blendMap;
	}
	float getHeightOfTerrain(float worldX, float worldZ);
	static float barryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos) {
		float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
		float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
		float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
		float l3 = 1.0f - l1 - l2;
		return l1 * p1.y + l2 * p2.y + l3 * p3.y;
	}
private:
	inline const static float SIZE = 800.0f;
	inline const static float MAX_HEIGHT = 40;
	inline const static int VERTEX_COUNT = 128;
	inline const static float MAX_PIXEL_COLOUR = 256 * 256 * 256;
	std::vector<std::vector<float>> heights;
	float x, z;
	RawModel model;
	TerrainTexturePack texturePack;
	TerrainTexture blendMap;
	RawModel generateTerrain(Loader loader, std::string heightMap);
	float getHeight(int x, int z, int height, unsigned char* image,SDL_Surface* image2);
	glm::vec3 calculateNormal(int x, int z, int height, unsigned char* image, SDL_Surface* image2);
};
RawModel Terrain::generateTerrain(Loader loader, std::string heightMap) {
	
	std::string fp = "./res/" + heightMap + ".png";
	int width, height, numComponents;
	//SDL_Surface* img = SDL_LoadBMP(fp);	//load the bmp image
	SDL_Surface *image= IMG_Load(fp.c_str());

	//if (!image) {
	//	printf("IMG_Load: %s\n", IMG_GetError());
	//	// handle error
	//}
	unsigned char* data = stbi_load((fp).c_str(), &width, &height, &numComponents, 4);
	if (data == NULL)
		std::cerr << "Unable to load texture: " << fp << std::endl;
	int VERTEX_COUNT = height;
	heights = std::vector<std::vector<float>>(VERTEX_COUNT, std::vector<float>(VERTEX_COUNT)); //new float[VERTEX_COUNT][VERTEX_COUNT];
	int count = VERTEX_COUNT * VERTEX_COUNT;
	std::vector<glm::vec3>vertices(count);
	std::vector<glm::vec3>normals(count);
	std::vector<glm::vec2>textureCoords(count);
	std::vector<int>indices(6 * (VERTEX_COUNT - 1)*(VERTEX_COUNT - 1));
	int vertexPointer = 0;
	for (int i = 0; i < VERTEX_COUNT; i++) {
		for (int j = 0; j < VERTEX_COUNT; j++) {
			glm::vec3 vertex;
			vertex.x = (float)j / (float)(VERTEX_COUNT - 1) * SIZE;
			float cheight = getHeight(j, i, height, data,image);
			heights[j][i] = cheight;
			vertex.y = cheight;
			vertex.z= (float)i / (float)(VERTEX_COUNT - 1) * SIZE;
			vertices[vertexPointer]=vertex;
			glm::vec3 normal = calculateNormal(j, i, height, data,image);
			normals[vertexPointer] = normal;//glm::vec3(0, 1, 0);
			glm::vec2 texC;
			texC.x = (float)j / ((float)VERTEX_COUNT - 1);
			texC.y = (float)i / ((float)VERTEX_COUNT - 1);
			textureCoords[vertexPointer]=texC;
			vertexPointer++;
		}
	}

	int pointer = 0;
	for (int gz = 0; gz<VERTEX_COUNT - 1; gz++) {
		for (int gx = 0; gx<VERTEX_COUNT - 1; gx++) {
			int topLeft = (gz*VERTEX_COUNT) + gx;
			int topRight = topLeft + 1;
			int bottomLeft = ((gz + 1)*VERTEX_COUNT) + gx;
			int bottomRight = bottomLeft + 1;
			indices[pointer++] = topLeft;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = topRight;
			indices[pointer++] = topRight;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = bottomRight;
		}
	}
	return loader.loadToVAO(vertices, textureCoords, normals, indices);
}

float Terrain::getHeightOfTerrain(float worldX, float worldZ) {
	float terrainX = worldX - this->x;
	float terrainZ = worldZ - this->z;
	float gridSquareSize = SIZE / ((float)heights.size() - 1);
	int gridX = (int)floor(terrainX / gridSquareSize);
	int gridZ = (int)floor(terrainZ / gridSquareSize);

	if (gridX >= heights.size() - 1 || gridZ >= heights.size() - 1
		|| gridX < 0 || gridZ < 0) {
		return 0;
	}
	float xCoord = fmodf(terrainX, gridSquareSize) / gridSquareSize;
	float zCoord = fmodf(terrainZ, gridSquareSize) / gridSquareSize;
	float answer;
	if (xCoord <= (1 - zCoord)) {

		answer = barryCentric(glm::vec3(0, heights[gridX][gridZ], 0), glm::vec3(1,
			heights[gridX + 1][gridZ], 0), glm::vec3(0,
				heights[gridX][gridZ + 1], 1), glm::vec2(xCoord, zCoord));
	}
	else {
		answer = barryCentric(glm::vec3(1, heights[gridX + 1][gridZ], 0), glm::vec3(1,
			heights[gridX + 1][gridZ + 1], 1), glm::vec3(0,
				heights[gridX][gridZ + 1], 1), glm::vec2(xCoord, zCoord));
	}
	return answer;
}
float Terrain::getHeight(int x, int z, int height, unsigned char* image, SDL_Surface* image2) {
	if (x< 0 || x >= height || z<0 || z >= height) {
		return 0;
	}
	unsigned bytePerPixel = 4;
	unsigned char* pixelOffset = image + (x + height * z) * bytePerPixel;
	unsigned char r = pixelOffset[0];
	unsigned char g = pixelOffset[1];
	unsigned char b = pixelOffset[2];
	unsigned char a = (bytePerPixel >= 4) ? pixelOffset[3] : 0xff;
	//Uint32 pixel = ((Uint32*)image2->pixels)[z*image2->pitch / 4 + x];
	//Uint8 r2, g2, b2;	//unsigned char
	//SDL_GetRGB(pixel, image2->format, &r2, &g2, &b2);
	//float h = (float)g;
	//h += MAX_PIXEL_COLOUR / 2.0f;
	//h /= MAX_PIXEL_COLOUR / 2.0f;
	/*h *= MAX_HEIGHT;*/
	//unsigned char color =
	//	((unsigned char*)image2->pixels)[4 * (z * image2->w + x)];
	float h = height * ((r / 255.0f) - 0.0f);
	return h;
	//if (channelCount == 4) // alpha channel
	//	unsigned char a = (channelCount >= 4) ? pixelOffset[3] : 0xff;
}
glm::vec3 Terrain::calculateNormal(int x, int z, int height, unsigned char* image, SDL_Surface* image2) {
	float heightL = getHeight(x - 1, z, height, image,image2);
	float heightR = getHeight(x + 1, z, height, image,image2);
	float heightD = getHeight(x, z - 1, height, image,image2);
	float heightU = getHeight(x, z + 1, height, image,image2);
	glm::vec3 normal(heightL - heightR, 2.0f, heightD - heightU);
	//normal.normalise();
	normal = glm::normalize(normal);
	return normal;
}

