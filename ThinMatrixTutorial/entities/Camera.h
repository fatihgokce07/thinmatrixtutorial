#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <SDL2/SDL.h>
#include "player.h"
#include "..\renderEngine\keyboard.h"
class Camera {
public:
	/*Camera(){}*/
	Camera(Player* player,Player& player2):player2(player2),player(player) {
		/*this->player = player;
		this->player2 = player2;*/
	}
	//Camera(const Camera& rhs) : player2(rhs.player2),player(rhs.player) // <-- here too!!
	//{
	//	std::cout << "A::copy" << std::endl;
	//}
	void move();
	glm::vec3 getPosition() {
		return position;
	}
	float getPitch() {
		return pitch;
	}
	float getYaw() {
		return yaw;
	}
	float getRoll() {
		return roll;
	}
private:
	SDL_Event e;
	glm::vec3 position = glm::vec3(0, 0, 0);
	float pitch=20;
	float yaw=0;
	float roll=0;
	float distanceFromPlayer = 30;
	float angleAroundPlayer = 0;
	
	Player* player;
	Player& player2;
	void calculateZoom();
	float calculateHorizontalDistance() {
		return (float)(distanceFromPlayer * glm::cos(glm::radians(pitch)));
	}
	float calculateVerticalDistance() {
		return (float)(distanceFromPlayer * glm::sin(glm::radians(pitch)));
	}
	void calculateCamaraPosition(float horizontalDistance, float verticalDistance);
	void calculatePitch();
	void calculateAngleAroundPlayer();
};
void Camera::move() {
	calculateZoom();
	calculatePitch();	
	calculateAngleAroundPlayer();
	float horizontalDistance = calculateHorizontalDistance();
	float verticalDistance = calculateVerticalDistance();
	calculateCamaraPosition(horizontalDistance, verticalDistance);
	yaw = 180 - (player->rotY + angleAroundPlayer);
	//int y = player2.rotY;
}
void Camera::calculateZoom() {
	if (Keyboard::startMouseWheel) {
	
		if (Keyboard::scroolUp)//scroll up
		{
			float zoomLevel = 120 * 0.1f;		
			distanceFromPlayer -= zoomLevel;		
		}
		else //scroll down
		{
			float zoomLevel = -120 * 0.1f;		
			distanceFromPlayer -= zoomLevel;		
		}
	
	}
	Keyboard::startMouseWheel = false;
	

	
}
void Camera::calculateCamaraPosition(float horizontalDistance, float verticalDistance) {
	float theta = player->rotY + angleAroundPlayer;
	float offsetX = (float)(horizontalDistance * glm::sin(glm::radians(theta)));
	float offsetZ = (float)(horizontalDistance * glm::cos(glm::radians(theta)));

	position.x = player->position.x - offsetX;
	position.z = player->position.z - offsetZ;
	position.y = player->position.y + verticalDistance;
}
void Camera::calculatePitch() {
	if (Keyboard::rightButtonClicked()) {
		//1 mean right button
		float pitchChange = Keyboard::getMouseDeltaY() * 0.1f;//how much mouse up down
		pitch -= pitchChange;
	}
}
void Camera::calculateAngleAroundPlayer() {
	if (Keyboard::leftButtonClicked()) {
		// 0 means left button
		float angleChange = Keyboard::getMouseDeltaX() * 0.1f;
		// how much the mouse moved left and right
		angleAroundPlayer -= angleChange;
	}
}
