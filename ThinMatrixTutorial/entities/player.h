#pragma once
#include <glm/glm.hpp>
#include "entity.h"
#include <SDL2/SDL.h>
#include "..\renderEngine\displayManager.h"
#include "..\renderEngine\keyboard.h"
#include "..\terrains\terrain.h"
class Player : public Entity
{
public:
	Player(){}
	Player(TexturedModel model, glm::vec3 position, float rotX, float rotY, float rotZ, float scale);
	void move(Terrain& terrain);

private:
	SDL_Event e;
	inline static const float RUN_SPEED = 20;
	inline static const float TURN_SPEED = 160;
	inline static const float GRAVITY = -50;
	inline static const float JUMP_POWER = 30;
	inline static float TERRAIN_HEIGHT = 0;
	float currentSpeed = 0;
	float currentTurnSpeed = 0;
	float upwardSpeed = 0;
	bool isInAir = false;
	void jump();
	void checkInputs();
};
Player::Player(TexturedModel model, glm::vec3 position, float rotX, float rotY, float rotZ, float scale): Entity(model, position, rotX, rotY, rotZ, scale) {
	//Entity::Entity(model, position, rotX, rotY, rotZ, scale);
	
}
void Player::move(Terrain& terrain) {
	checkInputs();
	float rs = currentTurnSpeed * DisplayManager::getFrameTimeSeconds();

	increaseRotation(0, rs, 0);
	float distance = currentSpeed * DisplayManager::getFrameTimeSeconds();
	float dx = (float)(distance * glm::sin(glm::radians(rotY)));
	float dz = (float)(distance * glm::cos(glm::radians(rotY)));
	increasePosition(dx, 0, dz);

	upwardSpeed += GRAVITY * DisplayManager::getFrameTimeSeconds();
	increasePosition(0, upwardSpeed * DisplayManager::getFrameTimeSeconds(), 0);
	float th = terrain.getHeightOfTerrain(position.x, position.z);
	if (position.y < th) {
		upwardSpeed = 0;
		isInAir = false;
		position.y = th;
	}
}
void Player::jump() {
	if (!isInAir) {
		this->upwardSpeed = JUMP_POWER;
		isInAir = true;
	}
}
void Player::checkInputs() {
	if (Keyboard::keyboards[SDLK_w]) {
		this->currentSpeed = RUN_SPEED;
	}
	else if (Keyboard::keyboards[SDLK_s] )
	{
		this->currentSpeed = -RUN_SPEED;
	}
	else {
		this->currentSpeed = 0;
	}
	if (Keyboard::keyboards[SDLK_d]) {
		this->currentTurnSpeed = TURN_SPEED;
	}
	else if (Keyboard::keyboards[SDLK_a])
	{
		this->currentTurnSpeed = -TURN_SPEED;
	}
	else {
		this->currentTurnSpeed = 0;
	}
	if ((Keyboard::keyboards[SDLK_SPACE]) ) {
		jump();
	}

}