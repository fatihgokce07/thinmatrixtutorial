#pragma once
#include <glm/glm.hpp>
#include "..\models\texturedModel.h"
class Entity {
public:
	TexturedModel model;
	glm::vec3 position;
	float rotX, rotY, rotZ;
	float scale;
	Entity(TexturedModel model, glm::vec3 position, float rotX, float rotY, float rotZ,
		float scale) {
		this->model = model;
		this->position = position;
		this->rotX = rotX;
		this->rotY = rotY;
		this->rotZ = rotZ;
		this->scale = scale;
	}
	Entity(TexturedModel model, glm::vec3 position,int index ,float rotX, float rotY, float rotZ,
		float scale) {
		this->model = model;
		this->position = position;
		this->rotX = rotX;
		this->rotY = rotY;
		this->rotZ = rotZ;
		this->scale = scale;
		this->textureIndex = index;
	}
	Entity(){}
	void increasePosition(float dx, float dy, float dz) {
		this->position.x += dx;
		this->position.y += dy;
		this->position.z += dz;
	}
	void increaseRotation(float dx, float dy, float dz) {
		this->rotX += dx;
		this->rotY += dy;
		this->rotZ += dz;
	}
	TexturedModel getTextureModel()  {
		return model;
	}
	float getTextureXOffset() {
		int column = textureIndex % model.getModelTextures()->numberOfRows;
		return (float)column / (float)model.getModelTextures()->numberOfRows;
	}
	float getTextureYOffset() {
		int row = textureIndex / model.getModelTextures()->numberOfRows;
		return (float)row / (float)model.getModelTextures()->numberOfRows;
	}
private:
	int textureIndex = 0;
};
