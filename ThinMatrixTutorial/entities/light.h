#pragma once
#include <glm/glm.hpp>
#include <vector>
struct Light {
public:
	glm::vec3 postion;
	glm::vec3 colour;
	Light(glm::vec3 position, glm::vec3 colour) {
		this->postion = position;
		this->colour = colour;
	}
private:

};