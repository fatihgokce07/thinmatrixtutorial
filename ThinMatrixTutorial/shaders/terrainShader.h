#include "ShaderProgram.h"
#pragma once

#include <glm/glm.hpp>
#include <string>
#include "..\entities\light.h"
#include "..\entities\Camera.h"
#include "..\toolbox\maths.h"
struct TerrainShader :public ShaderProgram {
public:
	TerrainShader();
	void loadShineVariables(float damper, float reflectivity);
	void loadTransformationMatrix(glm::mat4 matrix);
	void loadLight(Light light);
	void loadViewMatrix(Camera camera);
	void loadProjectionMatrix(glm::mat4 projection);
	void loadSkyColor(float r, float g, float b);
	void connectTextureUnits();
private:
	inline const static std::string VERTEX_FILE = "./res/terrainVertexShader.glsl";
	inline const static std::string FRAGMENT_FILE = "./res/terrainFragmentShader.glsl";
	int location_transformationMatrix;
	int location_projectionMatrix;
	int location_viewMatrix;
	int location_lightPosition;
	int location_lightColour;
	int location_shineDamper;
	int location_reflectivity;
	int location_skyColour;
    int location_backgroundTexture;
	int location_rTexture;
	int location_gTexture;
	int location_bTexture;
	int location_blendMap;
protected:
	void bindAttributes() {
		ShaderProgram::bindAttribute(0, "position");
		bindAttribute(1, "textureCoordinates");
		bindAttribute(2, "normal");
	}
	void getAllUniformLocations();
};
TerrainShader::TerrainShader() :ShaderProgram(VERTEX_FILE, FRAGMENT_FILE) {
	bindAttributes();
	getAllUniformLocations();
}
void TerrainShader::getAllUniformLocations() {
	location_transformationMatrix = getUniformLocation("transformationMatrix");
	location_projectionMatrix = getUniformLocation("projectionMatrix");
	location_viewMatrix = getUniformLocation("viewMatrix");
	location_lightPosition = getUniformLocation("lightPosition");
	location_lightColour = getUniformLocation("lightColour");
	location_shineDamper = getUniformLocation("shineDamper");
	location_reflectivity = getUniformLocation("reflectivity");
	location_skyColour = getUniformLocation("skyColour");

	location_backgroundTexture = getUniformLocation("backgroundTexture");
	location_rTexture = getUniformLocation("rTexture");
	location_gTexture = getUniformLocation("gTexture");
	location_bTexture = getUniformLocation("bTexture");
	location_blendMap = getUniformLocation("blendMap");
}
void TerrainShader::loadShineVariables(float damper, float reflectivity) {
	loadFloat(location_shineDamper, damper);
	loadFloat(location_reflectivity, reflectivity);
}

void TerrainShader::loadTransformationMatrix(glm::mat4 matrix) {
	loadMatrix(location_transformationMatrix, matrix);
}

void TerrainShader::loadLight(Light light) {
	loadVector(location_lightPosition, light.postion);
	loadVector(location_lightColour, light.colour);
}

void TerrainShader::loadViewMatrix(Camera camera) {
	glm::mat4 viewMatrix = Maths::createViewMatrix(camera);
	loadMatrix(location_viewMatrix, viewMatrix);
}

void TerrainShader::loadProjectionMatrix(glm::mat4 projection) {
	loadMatrix(location_projectionMatrix, projection);
}
void TerrainShader::loadSkyColor(float r, float g, float b) {
	loadVector(location_skyColour, glm::vec3(r, g, b));
}
void TerrainShader::connectTextureUnits() {
	loadInt(location_backgroundTexture, 0);
	loadInt(location_rTexture, 1);
	loadInt(location_gTexture, 2);
	loadInt(location_bTexture, 3);
	loadInt(location_blendMap, 4);
}
