#include "ShaderProgram.h"
#pragma once
#include <glm/glm.hpp>
#include "../entities/Camera.h"
#include "../toolbox/maths.h"
#include "../entities/light.h"
class StaticShader: public ShaderProgram {
public:
	StaticShader();
	void connectTextureUnits();
	void loadTransformationMatrix(glm::mat4 matrix);
	void loadProjectionMatrix(glm::mat4 matrix);
	void loadViewMatrix(Camera camera);
	void loadLight(Light light);
	void loadShineVariables(float damper,float reflectivity);
	void loadFakeLightingVariable(bool useFake);
	void loadSkyColor(float r, float g, float b);
	void loadNumberOfRows(int numberOfRows);
	void loadOffset(float x, float y);
private:
	const static  std::string VERTEX_FILE;
	const static  std::string FRAGMENT_FILE;
	int location_texture1;
	int location_texture2;
	int location_transformationMatrix;
	int location_projectionMatrix;
	int location_viewMatrix;
	int location_lightPosition;
	int location_lightColour;
	int location_shineDamper;
	int location_reflectivity;
	int location_useFakeLighting;
	int location_skyColour;
	int location_numberOfRows;
	int location_offset;

protected:
	void bindAttributes() {
		ShaderProgram::bindAttribute(0, "position");
		bindAttribute(1, "textureCoordinates");
		bindAttribute(2, "normal");
	}
	void getAllUniformLocations();
};
const std::string StaticShader::VERTEX_FILE = "./res/vertexShader.glsl";
const std::string StaticShader::FRAGMENT_FILE = "./res/fragmentShader.glsl";

StaticShader::StaticShader() :ShaderProgram(VERTEX_FILE, FRAGMENT_FILE) {
	bindAttributes();
	getAllUniformLocations();
};
void StaticShader::getAllUniformLocations() {
	location_texture1 = ShaderProgram::getUniformLocation("texture1");
	location_texture2 = ShaderProgram::getUniformLocation("texture2");
	location_transformationMatrix = getUniformLocation("transformationMatrix");
	location_projectionMatrix = getUniformLocation("projectionMatrix");
	location_viewMatrix = getUniformLocation("viewMatrix");

	location_lightPosition = getUniformLocation("lightPosition");
	location_lightColour = getUniformLocation("lightColour");

	location_shineDamper = getUniformLocation("shineDamper");
	location_reflectivity = getUniformLocation("reflectivity");
	location_useFakeLighting = getUniformLocation("useFakeLighting");
	location_skyColour = getUniformLocation("skyColour");
	location_numberOfRows = getUniformLocation("numberOfRows");
	location_offset = getUniformLocation("offset");
}
void StaticShader::connectTextureUnits() {
	loadInt(location_texture1, 0);
	//loadInt(location_texture2, 1);
}

void StaticShader::loadTransformationMatrix(glm::mat4 matrix) {
	loadMatrix(location_transformationMatrix, matrix);
}

void StaticShader::loadProjectionMatrix(glm::mat4 matrix) {
	loadMatrix(location_projectionMatrix, matrix);
}
void StaticShader::loadViewMatrix(Camera camera) {
	glm::mat4 matrix = Maths::createViewMatrix(camera);
	loadMatrix(location_viewMatrix, matrix);
}
void StaticShader::loadLight(Light light) {
	loadVector(location_lightPosition, light.postion);
	loadVector(location_lightColour, light.colour);
}
void StaticShader::loadShineVariables(float damper, float reflectivity) {
	loadFloat(location_shineDamper, damper);
	loadFloat(location_reflectivity, reflectivity);
}
//void StaticShader::bindAttributes() {
//	bindAttribute(0, "position");
//}
void StaticShader::loadFakeLightingVariable(bool useFake) {
	loadBoolean(location_useFakeLighting, useFake);
}
void StaticShader::loadSkyColor(float r, float g, float b) {
	loadVector(location_skyColour, glm::vec3(r, g, b));
}
void StaticShader::loadNumberOfRows(int numberOfRows) {
	loadFloat(location_numberOfRows, numberOfRows);
}
void StaticShader::loadOffset(float x, float y) {
	load2DVector(location_offset, glm::vec2(x, y));
}