#include <string>
#include <GL/glew.h>
#include <iostream>
#include <glm/glm.hpp>
#pragma once
class  ShaderProgram
{
public:
	 ShaderProgram(std::string vertexFile,std::string fragmentFile);
	virtual ~ ShaderProgram();
	void start() { glUseProgram(programID); }
	void stop() { glUseProgram(0); }
	void clenaUp();
private:
	int programID;
	int vertexShaderID;
	int fragmentShaderID;
	int static loadShader(const std::string& fileName, int type);
protected:
	virtual void bindAttributes() = 0;
	virtual void getAllUniformLocations() = 0;
	void bindAttribute(int attribute,const std::string& variableName) {
		glBindAttribLocation(programID, attribute, variableName.c_str());
	}
	int getUniformLocation(const char* uniformName) {
		return glGetUniformLocation(programID, uniformName);
	}
	void loadInt(int location, int value) {
		glUniform1i(location, value);
	}
	void loadFloat(int location, float value) {
		glUniform1f(location, value);
	}
	
	void loadVector(int location, glm::vec3 vector) {
		glUniform3f(location,vector.x,vector.y,vector.z);
	}
	void loadBoolean(int location, bool value) {
		float toLoad = 0;
		if (value) {
			toLoad = 1;
		}
		loadFloat(location, toLoad);
	}
	void loadMatrix(int location, glm::mat4 matrix) {
		glUniformMatrix4fv(location, 1, GL_FALSE, &matrix[0][0]);
	}
	void load2DVector(int location, glm::vec2 vector) {
		glUniform2f(location, vector.x, vector.y);
	}
};

 ShaderProgram:: ShaderProgram(std::string vertexFile, std::string fragmentFile)
{
	 //std::function<double(double, double)> f = [](double a, double b) { return a * b };
	 vertexShaderID = loadShader(vertexFile, GL_VERTEX_SHADER);
	 fragmentShaderID = loadShader(fragmentFile, GL_FRAGMENT_SHADER);
	 this->programID = glCreateProgram();
	
	 glAttachShader(programID, vertexShaderID);
	 glAttachShader(programID, fragmentShaderID);
	// bindAttributes();
	 glLinkProgram(programID);
	 glValidateProgram(programID);
	
}

 ShaderProgram::~ ShaderProgram()
{
}
 void ShaderProgram::clenaUp() {
	 stop();
	 glDetachShader(programID, vertexShaderID);
	 glDetachShader(programID, fragmentShaderID);
	 glDeleteShader(vertexShaderID);
	 glDeleteShader(fragmentShaderID);
	 glDeleteProgram(programID);
 }
 int ShaderProgram::loadShader(const std::string& fileName, int type) {
	 std::ifstream reader;
	 
	 std::string line;
	 std::string shaderSource;
	 try {
		 reader.open((fileName).c_str());
		 if (reader.is_open()) {
			 while (reader.good()) {
				 getline(reader, line);
				 shaderSource.append(line + "\n");
			 }
		 }
		 else {
			 std::cout << "Unable load shader " << fileName << std::endl;
		 }
		
	 }
	 catch (const std::exception& e) { // reference to the base of a polymorphic object
		 std::cout << e.what(); 
		 //exit(0);
	 }
	 int shaderId = glCreateShader(type);
	 char const* vertexSourcePointer = shaderSource.c_str();
	 glShaderSource(shaderId, 1, &vertexSourcePointer, NULL);
	 GLint success = 0;
	 GLchar error[1024] = { 0 };
	 glCompileShader(shaderId);
	 glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
	 if (success == GL_FALSE) {
		 glGetShaderInfoLog(shaderId, sizeof(error), NULL, error);
		 std::cerr << "Error compiling shader" << ": '" << error << "'" << std::endl;
		 //exit(0);
	 }
	 return shaderId;
 }